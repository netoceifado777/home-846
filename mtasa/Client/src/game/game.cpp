/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "game.h"

game::game()
{
    LOG("initializing game interface..");
}

extern char* WORLD_PLAYERS;
uintptr_t game::FindPlayer() {
    return *(uintptr_t*)WORLD_PLAYERS;
}

void game::toggleRadar(bool iToggle)
{
	*(uint8_t*)(SA(0x8EF36B)) = (uint8_t)!iToggle;
}

void game::displayHUD(bool bDisp)
{
	*(uint8_t*)(SA(0x7165E8)) = bDisp ? 1 : 0;
	toggleRadar(bDisp ? 1 : 0);
}

void game::displayWidgets(bool bDisp)
{
    // CTouchInterface::DrawAll
    WriteMemory(SA(0x0026EE80), bDisp ? (uintptr_t)"\xF8\xB5" : (uintptr_t)"\xF7\x46", 2);
}