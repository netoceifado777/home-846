/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "../core/core.h"
#include "gui.h"
#include "window.h"

extern window *pMainWindow;

int mainmenu::g_iCurrentMenuItemSel = 0;
uint mainmenu::g_iSpecialBrowserType = -1;
float mainmenu::m_iXOff, mainmenu::m_iYOff;
float mainmenu::m_iMenuSizeX, mainmenu::m_iMenuSizeY;
bool mainmenu::g_bMenuActive = true;
bool mainmenu::g_bAboutActive = false;
bool mainmenu::g_bSettingsActive = false;

#define NATIVE_RES_X    1280.0f
#define NATIVE_RES_Y    1024.0f

#define NATIVE_BG_X     1280.0f
#define NATIVE_BG_Y     649.0f

#define NATIVE_LOGO_X     1058.0f
#define NATIVE_LOGO_Y     540.0f

#define CORE_MTA_MENUITEMS_START_X  0.168

#define CORE_MTA_BG_MAX_ALPHA       1.00f   //ACHTUNG: Set to 1 for now due to GTA main menu showing through (no delay inserted between Entering game... and loading screen)
#define CORE_MTA_BG_INGAME_ALPHA    0.90f
#define CORE_MTA_FADER              0.05f // 1/20
#define CORE_MTA_FADER_CREDITS      0.01f

#define CORE_MTA_HOVER_SCALE        1.0f
#define CORE_MTA_NORMAL_SCALE       0.6f
#define CORE_MTA_HOVER_ALPHA        1.0f
#define CORE_MTA_NORMAL_ALPHA       0.6f

#define CORE_MTA_HIDDEN_ALPHA       0.0f
#define CORE_MTA_DISABLED_ALPHA     0.4f
#define CORE_MTA_ENABLED_ALPHA      1.0f

#define CORE_MTA_ANIMATION_TIME     200
#define CORE_MTA_MOVE_ANIM_TIME     600

#define MENU_ITEM_QUICK_CONNECT     1
#define MENU_ITEM_SERVER_BROWSER    2
#define MENU_ITEM_HOST_GAME         3
#define MENU_ITEM_MAP_EDITOR        4
#define MENU_ITEM_SETTINGS          5
#define MENU_ITEM_ABOUT             6
#define MENU_ITEM_EXIT              7

typedef struct ScreenVector {
    float fX, fY, fI;
};

RwTexture* bg_texture = 0;
RwTexture* bglogo_texture = 0;

void mainmenu::initialise()
{
    // Adjust window size to resolution
    float ScreenSizeX = RsGlobal->maximumWidth;
    float ScreenSizeY = RsGlobal->maximumHeight;

    // FileLogger::debugLog("screen vector > x: %f y: %f", ScreenSizeX, ScreenSizeY);

    int iBackgroundX = 0;
    int iBackgroundY = 0;
    int iBackgroundSizeX = ScreenSizeX;
    int iBackgroundSizeY;

    // First let's work out our x and y offsets
    if (ScreenSizeX > ScreenSizeY)            // If the monitor is a normal landscape one
    {
        float iRatioSizeY = ScreenSizeY / NATIVE_RES_Y;
        m_iMenuSizeX = NATIVE_RES_X * iRatioSizeY;
        m_iMenuSizeY = ScreenSizeY;
        m_iXOff = (ScreenSizeX - m_iMenuSizeX) * 0.5f;
        m_iYOff = 0;

        float iRatioSizeX = ScreenSizeX / NATIVE_RES_X;
        iBackgroundSizeX = ScreenSizeX;
        iBackgroundSizeY = NATIVE_BG_Y * iRatioSizeX;
    }
    else            // Otherwise our monitor is in a portrait resolution, so we cant fill the background by y
    {
        float iRatioSizeX = ScreenSizeX / NATIVE_RES_X;
        m_iMenuSizeY = NATIVE_RES_Y * iRatioSizeX;
        m_iMenuSizeX = ScreenSizeX;
        m_iXOff = 0;
        m_iYOff = (ScreenSizeY - m_iMenuSizeY) * 0.5f;

        iBackgroundY = m_iYOff;
        iBackgroundSizeX = m_iMenuSizeX;
        iBackgroundSizeY = NATIVE_BG_Y * iRatioSizeX;
    }

    bg_texture = (RwTexture*)utilities::loadTextureFromDB("txd", "background");
    bglogo_texture = (RwTexture*)utilities::loadTextureFromDB("txd", "background_logo");
}

typedef struct LOGOVector {
    float fX, fY;
};

void mainmenu::draw()
{
    if(!mainmenu::g_bMenuActive) {
        return;
    }

    ImVec2 vecWindowInfoPos = ImVec2(pMainWindow->g_fPosX, pMainWindow->g_fPosY);
    ImVec2 vecWindowInfoSize = ImVec2(pMainWindow->g_fPosX + pMainWindow->g_fSizeX, pMainWindow->g_fPosY + pMainWindow->g_fSizeY);
    
    stRect rect;
    stfRect uv;

    // background texture
    if(bg_texture) 
    {
        ImGuiWrapper::CreateSTFRect(&rect, &uv, 0.0f, 0.0f, static_cast<float>(RsGlobal->maximumWidth), static_cast<float>(RsGlobal->maximumHeight), 0.0f, 0.0f, 1.0f, 1.0f);
	    ImGuiWrapper::DrawRaster(&rect, 0xFFFFFFFF, bg_texture->raster, &uv);
    }

    // background fill
    ImGuiWrapper::CreateSTFRect(&rect, &uv, 0.0f, 0.0f, static_cast<float>(RsGlobal->maximumWidth), static_cast<float>(RsGlobal->maximumHeight), 0.0f, 0.0f, 1.0f, 1.0f);
	ImGuiWrapper::DrawRaster(&rect, 0xB5000000);

    // logo image
	LOGOVector logoSize = {NATIVE_LOGO_X+(RsGlobal->maximumWidth / 4), (NATIVE_LOGO_Y / NATIVE_RES_Y) * m_iMenuSizeY};
    LOGOVector logoPos = {(RsGlobal->maximumWidth / 2) - (logoSize.fX / 2), 0.365f * m_iMenuSizeY - logoSize.fY / 2};

    if(bglogo_texture) 
    {
        float f1W = RsGlobal->maximumWidth * 0.22;
	    float f2W = RsGlobal->maximumWidth - f1W;

        ImVec2 a = ImVec2(f1W, logoPos.fY - 60.0f);
		ImVec2 b = ImVec2(f2W, logoSize.fY - 60.0f);
		ImGui::GetBackgroundDrawList()->AddImage((ImTextureID)bglogo_texture->raster, a, b);
    }

    float fCustomFontSize = 45.0f;

    // menu items
    ImVec2 vecTextInfo = ImVec2(RsGlobal->maximumWidth * 0.27, (pMainWindow->g_fPosY + pMainWindow->g_fSizeY/2) - ImGui::CalcTextSize("QWERTYABOPASNBEQEW").y/2);

    float fSelectFontSizeMultiply = 1.25f;
    float fSizeOffset = 20.0f;
    bool bTabSnapEffect = false;
    bool bUseZ = true;
    bool bUseOutline = false;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 1) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 1 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 1 && bTabSnapEffect ? "\tQUICK CONNECT" : "QUICK CONNECT");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 1 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 2) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 2 ? fCustomFontSize * fSelectFontSizeMultiply: fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 2 && bTabSnapEffect ? "\tSERVER BROWSER" : "SERVER BROWSER");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 2 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 3) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 3 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 3 && bTabSnapEffect ? "\tHOST GAME" : "HOST GAME");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 3 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 4) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 4 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 4 && bTabSnapEffect ? "\tMAP EDITOR" : "MAP EDITOR");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 4 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 5) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 5 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 5 && bTabSnapEffect ? "\tSETTINGS" : "SETTINGS");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 5 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 6) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 6 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 6 && bTabSnapEffect ? "\tABOUT" : "ABOUT");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 6 && bUseZ ? fCustomFontSize * fSelectFontSizeMultiply + fSizeOffset : fCustomFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 7) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 7 ? fCustomFontSize * fSelectFontSizeMultiply : fCustomFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 7 && bTabSnapEffect ? "\tEXIT" : "EXIT");

    mainmenu::renderServerBrowser();
    mainmenu::renderAbout();
    mainmenu::renderSettings();
}

void mainmenu::pushTouch(float x, float y)
{
    if(!mainmenu::g_bMenuActive || mainmenu::g_iSpecialBrowserType != -1 || mainmenu::g_bAboutActive || mainmenu::g_bSettingsActive) {
        return;
    }

    if(y > 500.0f && y < 575.0f) {
        mainmenu::g_iCurrentMenuItemSel = 1;
    }
    else if(y > 575.0f && y < 650.0f) {
        mainmenu::g_iCurrentMenuItemSel = 2;
    }
    else if(y > 650.0f && y < 725.0f) {
        mainmenu::g_iCurrentMenuItemSel = 3;
    }
    else if(y > 725.0f && y < 800.0f) {
        mainmenu::g_iCurrentMenuItemSel = 4;
    }
    else if(y > 800.0f && y < 875.0f) {
        mainmenu::g_iCurrentMenuItemSel = 5;
    }
    else if(y > 875.0f && y < 950.0f) {
        mainmenu::g_iCurrentMenuItemSel = 6;
    }
    else if(y > 950.0f && y < 1025.0f) {
        mainmenu::g_iCurrentMenuItemSel = 7;
    }
    else
    {
        mainmenu::g_iCurrentMenuItemSel = 0;
    }
}

void mainmenu::toggleRender()
{
    mainmenu::g_bMenuActive ^= true;
    pMainWindow->setPadLocked(mainmenu::g_bMenuActive);
}

void mainmenu::editStyle()
{
    ImGuiStyle* style = &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

    style->WindowPadding    = ImVec2(8, 8);
    style->WindowBorderSize = 0.0f;
    style->WindowRounding   = 15.0f;
	style->ChildBorderSize  = 2.0f;
    style->FrameBorderSize  = 2.0f;
	
    colors[ImGuiCol_TitleBg]                = ImVec4(RGBA_TO_FLOAT(20, 20, 36, 255));
    colors[ImGuiCol_TitleBgActive]          = ImVec4(RGBA_TO_FLOAT(20, 20, 36, 255));
    colors[ImGuiCol_TitleBgCollapsed]       = ImVec4(RGBA_TO_FLOAT(20, 20, 36, 255));
	colors[ImGuiCol_Border]                 = ImVec4(RGBA_TO_FLOAT(255, 255, 255, 255));
    colors[ImGuiCol_BorderShadow]           = ImVec4(RGBA_TO_FLOAT(255, 255, 255, 255));
    colors[ImGuiCol_Button]                 = ImVec4(RGBA_TO_FLOAT(27, 24, 20, 255));
    colors[ImGuiCol_ButtonHovered]          = ImVec4(RGBA_TO_FLOAT(19, 17, 26, 255));
    colors[ImGuiCol_ButtonActive]           = ImVec4(RGBA_TO_FLOAT(19, 17, 26, 255));
	colors[ImGuiCol_ChildBg]                = ImVec4(RGBA_TO_FLOAT(5, 5, 9, 255));
    colors[ImGuiCol_PopupBg]                = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
}

void mainmenu::showButtonBorder(bool s)
{
    ImGuiStyle* style = &ImGui::GetStyle();

    if(s) 
    {
        style->FrameBorderSize = 2.0f;
    }
    else
    {
        style->FrameBorderSize = 0.0f;
    }
}

void mainmenu::showChildBorder(bool s)
{
    ImGuiStyle* style = &ImGui::GetStyle();

    if(s) 
    {
        style->ChildBorderSize = 2.0f;
    }
    else
    {
        style->ChildBorderSize = 0.0f;
    }
}

void mainmenu::renderSettings()
{
    if(!mainmenu::g_bSettingsActive) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    mainmenu::editStyle();

    ImGuiStyle* style = &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

    float fX = io.DisplaySize.x / 2.25f;
    float fY = io.DisplaySize.y / 1.25f;

    ImGui::SetNextWindowPos(ImVec2((io.DisplaySize.x / 2) - (fX / 2), (io.DisplaySize.y / 2) - (fY / 2)), ImGuiCond_Once);
    ImGui::SetNextWindowSize(ImVec2(fX, fY), ImGuiCond_Once);

    style->ChildBorderSize  = 0.0f;
    style->FrameBorderSize  = 0.0f;
        
    ImGui::SetNextWindowSizeConstraints(
    ImVec2(0, -1), ImVec2(std::numeric_limits<float>::max(), -1));
    {
        ImGui::Begin("Settings", nullptr,
                       ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus); 
        
        ImGui::Button("MultiPlayer");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Video");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Audio");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Binds");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Controls");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Interface");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Web Browser");
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::Button("Advanced");

        ImGui::BeginChild("settingschild", ImVec2(ImGui::GetWindowContentRegionWidth() - 1.0f, ImGui::GetWindowHeight() - 200.0f));

        ImGui::Text("soon");

        ImGui::EndChild();

        if(ImGui::Button("Back", ImVec2(200, 80)))
        {
            mainmenu::showSettings(false);
        }

        ImGui::End(); 
    }
}

void mainmenu::renderAbout()
{
    if(!mainmenu::g_bAboutActive) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    mainmenu::editStyle();

    ImGuiStyle* style = &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

    float fX = io.DisplaySize.x / 2.25f;
    float fY = io.DisplaySize.y / 1.25f;

    ImGui::SetNextWindowPos(ImVec2((io.DisplaySize.x / 2) - (fX / 2), (io.DisplaySize.y / 2) - (fY / 2)), ImGuiCond_Once);
    ImGui::SetNextWindowSize(ImVec2(fX, fY), ImGuiCond_Once);

    style->ChildBorderSize  = 0.0f;
    style->FrameBorderSize  = 0.0f;
        
    ImGui::SetNextWindowSizeConstraints(
    ImVec2(0, -1), ImVec2(std::numeric_limits<float>::max(), -1));
    {
        ImGui::Begin("== Multi Theft Auto: San Andreas | HOME 846 ==", nullptr,
                       ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus); 

        ImGui::BeginChild("aboutchild", ImVec2(ImGui::GetWindowContentRegionWidth() - 1.0f, ImGui::GetWindowHeight() - 150.0f));
        
        // ImGui::SetCursorPosX(ImGui::GetCursorPosX() + ((ImGui::GetWindowWidth() / 2) - (ImGui::CalcTextSize("This software makes use of the following libraries and software:").x)));
        ImGui::Text("Programming");
        ImGui::Text("alycardinal");
        ImGui::Text("\n");
        ImGui::Text("Contributors");
        ImGui::Text("none");
        ImGui::Text("\n");
        ImGui::Text("Game Design / Scripting");
        ImGui::Text("alycardinal");
        ImGui::Text("\n");
        ImGui::Text("Language Localization");
        ImGui::Text("alycardinal");
        ImGui::Text("\n");
        ImGui::Text("Patch contributors");
        ImGui::Text("none");
        ImGui::Text("\n");
        ImGui::Text("Special Thanks");
        ImGui::Text("HOME 846 VK/Discord community");
        ImGui::Text("\n");
        ImGui::Text("This software makes use of the following libraries and software:");
        ImGui::Text("RakNet (http://www.jenkinssoftware.com/)");
        ImGui::Text("CEGUI (http://www.cegui.org.uk/)");
        ImGui::Text("cURL (http://curl.haxx.se/)");
        ImGui::Text("libpcre (http://www.pcre.org/)");
        ImGui::Text("Lua (http://www.lua.org/)");
        ImGui::Text("SQLite (http://www.sqlite.org/)");
        ImGui::Text("libpng (http://www.libpng.org/)");
        ImGui::Text("Embedded HTTP Server (http://ehs.fritz-elfert.de/)");
        ImGui::Text("zlib (http://zlib.net/)");
        ImGui::Text("bzip2 (http://bzip.org/)");
        ImGui::Text("UnRAR (http://www.rarlab.com/)");
        ImGui::Text("tinygettext (https://github.com/tinygettext/tinygettext/)");
        ImGui::Text("PortAudio (http://www.portaudio.com/)");
        ImGui::Text("speex (http://www.speex.org/)");
        ImGui::Text("breakpad (https://chromium.googlesource.com/breakpad/breakpad/)");
        ImGui::Text("CEF (https://bitbucket.org/chromiumembedded/cef/)");
        ImGui::Text("inspect.lua by kikito (https://github.com/kikito/inspect.lua)");

        ImGui::EndChild();

        if(ImGui::Button("Back", ImVec2(200, 80)))
        {
            mainmenu::showAbout(false);
        }

        ImGui::End(); 
    }
}

void mainmenu::renderServerBrowser()
{
    ImGuiIO& io = ImGui::GetIO();

    if(mainmenu::g_iSpecialBrowserType != -1)
    {
        ImGuiStyle* style = &ImGui::GetStyle();
	    ImVec4* colors = style->Colors;

        mainmenu::editStyle();

        float fX = io.DisplaySize.x / 1.25f;
        float fY = io.DisplaySize.y / 1.25f;

        ImGui::SetNextWindowPos(ImVec2((io.DisplaySize.x / 2) - (fX / 2), (io.DisplaySize.y / 2) - (fY / 2)), ImGuiCond_Once);
        ImGui::SetNextWindowSize(ImVec2(fX, fY), ImGuiCond_Once);

        style->ChildBorderSize  = 0.0f;
        style->FrameBorderSize  = 0.0f;
        
        ImGui::SetNextWindowSizeConstraints(
        ImVec2(0, -1), ImVec2(std::numeric_limits<float>::max(), -1));
        {
            ImGui::Begin("Server browser", nullptr,
                       ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus); 

            style->ChildBorderSize  = 2.0f;
            style->FrameBorderSize  = 2.0f;

            ImVec2 vecSize = ImVec2(ImGui::CalcTextSize("QWERTYUIOP").x + 1.0f, ImGui::CalcTextSize("QWERTYUIOP").y + 15.0f);

            float fSameSize = 15.0f;

            mainmenu::showButtonBorder(mainmenu::g_iSpecialBrowserType == 1 ? true : false);
            if(ImGui::Button("Internet", vecSize)) mainmenu::g_iSpecialBrowserType = 1;
            ImGui::SameLine(0, fSameSize);

            mainmenu::showButtonBorder(mainmenu::g_iSpecialBrowserType == 0 ? true : false);
            if(ImGui::Button("Local", vecSize)) mainmenu::g_iSpecialBrowserType = 0;
            ImGui::SameLine(0, fSameSize);

            mainmenu::showButtonBorder(mainmenu::g_iSpecialBrowserType == 2 ? true : false);
            if(ImGui::Button("Favourites", vecSize)) mainmenu::g_iSpecialBrowserType = 2;
            ImGui::SameLine(0, fSameSize);

            mainmenu::showButtonBorder(mainmenu::g_iSpecialBrowserType == 3 ? true : false);
            if(ImGui::Button("Recent", vecSize)) mainmenu::g_iSpecialBrowserType = 3;

            mainmenu::showButtonBorder(false);
            ImGui::ItemSize(ImVec2(0.0f, 15.0f));
            
            mainmenu::showButtonBorder(true);
            ImVec4 sClText = colors[ImGuiCol_Text];
            ImVec4 sClFrame = colors[ImGuiCol_FrameBg];

            colors[ImGuiCol_Text]                   = ImVec4(RGBA_TO_FLOAT(0, 0, 0, 255));
            colors[ImGuiCol_FrameBg]                = ImVec4(RGBA_TO_FLOAT(255, 255, 255, 255));
 
            static bool bFirstAvali = false;
            if(!bFirstAvali)
            {
                keyboard::setText("mtasa://debug");
                bFirstAvali = true;
            }

            ImGui::InputTextMultiline("", keyboard::getText(), 512, ImVec2(ImGui::GetWindowContentRegionWidth() / 2.25f, vecSize.y + 5.0f), ImGuiInputTextFlags_NoHorizontalScroll);
            mainmenu::showButtonBorder(false);

            colors[ImGuiCol_Text] = sClText;
            colors[ImGuiCol_FrameBg] = sClFrame;

            if (ImGui::IsItemClicked()) {
                keyboard::setVisibility(true);
            }

            ImGui::SameLine(0, fSameSize);

            mainmenu::showButtonBorder(true);
            if(ImGui::Button("Connect", ImVec2(vecSize.x, vecSize.y + 5.0f)))
            {
                if(strncmp(keyboard::getText(), "mtasa://debug", 512) == 0 || strncmp(keyboard::getText(), "mtasa://debug-second", 512) == 0) 
                {
                    Config::Load("mtasa.ini");

                    char conBuffer[0xFF] = { 0 };
                    sprintf(conBuffer, "%s:%s:%s:0", Config::GetValueString("mtasa-cfg-10", "server_address"), Config::GetValueString("mtasa-cfg-10", "server_port"), Config::GetValueString("mtasa-cfg-10", "client_username"));
                    
                    // _Z14secure_connectPKc
                    ((void (*)(const char*))(NM(utilities::findMethod("libnetwork_module.so", "_Z14secure_connectPKc"))))(conBuffer);
                    
                    Config::Unload();
                    
                    mainmenu::toggleRender();
                    bFirstAvali = false;
                }
            }
            
            static uint g_iBrowse = 0;
            static uint g_iMaxBrowse = 1;

            ImGui::SameLine(0, fSameSize);

            if(ImGui::Button("UP", ImVec2(vecSize.x, vecSize.y + 5.0f)))
            {
                if(g_iBrowse > 0) {
                    g_iBrowse--;
                    if(g_iBrowse == 0)
                    {
                        keyboard::setText("mtasa://debug");
                    }
                    else if(g_iBrowse == 1)
                    {
                        keyboard::setText("mtasa://debug-second");
                    }
                }
            }

            ImGui::SameLine(0, fSameSize);

            if(ImGui::Button("DOWN", ImVec2(vecSize.x, vecSize.y + 5.0f)))
            {
                if(g_iBrowse < g_iMaxBrowse) {
                    g_iBrowse++;
                    if(g_iBrowse == 0)
                    {
                        keyboard::setText("mtasa://debug");
                    }
                    else if(g_iBrowse == 1)
                    {
                        keyboard::setText("mtasa://debug-second");
                    }
                }
            }

            mainmenu::showButtonBorder(false);

            float fChildHeight = (fY / 1.45f) - 30.0f;

            ImGui::ItemSize(ImVec2(0.0f, 15.0f));

            colors[ImGuiCol_ChildBg] = ImVec4(RGBA_TO_FLOAT(5, 5, 9, 200));
            ImGui::BeginChild("global-list", ImVec2(ImGui::GetWindowContentRegionWidth() - 30.0f, fChildHeight), true);
            ImGui::ItemSize(ImVec2(0.0f, 5.0f));

            colors[ImGuiCol_ChildBg] = ImVec4(RGBA_TO_FLOAT(5, 5, 9, 255));
            
            ImGui::Columns(1, NULL);

            { // style
                colors[ImGuiCol_Border]                 = colors[ImGuiCol_Button];
                colors[ImGuiCol_BorderShadow]           = colors[ImGuiCol_Button];
                style->ChildBorderSize = 1.0f;
            }

            ImGui::SetCursorPosX(ImGui::GetCursorPosX() + 15.0f);

            ImGui::BeginChild("global-list_part_a", ImVec2(ImGui::GetWindowContentRegionWidth() / 1.25f - 30.0f, fChildHeight - 30.0f), true);

            ImVec4 tmpCol = colors[ImGuiCol_Button];
            colors[ImGuiCol_Button]                 = ImVec4(RGBA_TO_FLOAT(0, 0, 0, 0));

            ImGui::Columns(5, NULL);
            ImGui::Text(" ");
            ImGui::Button(g_iBrowse == 0 ? "> 1.5" : "1.5");
            ImGui::Button(g_iBrowse == 1 ? "> 1.5" : "1.5");
            ImGui::NextColumn();
            ImGui::Text("Name");
            ImGui::Button("MTA:SA Debug #1");
            ImGui::Button("MTA:SA Debug #2");
            ImGui::NextColumn();
            ImGui::Text("Players");
            ImGui::Button("0/200");
            ImGui::Button("0/200");
            ImGui::NextColumn();
            ImGui::Text("Ping");
            ImGui::Button("30");
            ImGui::Button("30");
            ImGui::NextColumn();
            ImGui::Text("Gamemode");
            ImGui::Button("MTA:SA");
            ImGui::Button("MTA:SA");
            ImGui::NextColumn();

            ImGui::EndChild(); // global-list_part_a
            ImGui::SameLine(0, fSameSize);
            ImGui::NextColumn();

            ImGui::BeginChild("global-list_part_b", ImVec2(ImGui::GetWindowContentRegionWidth() / 5, fChildHeight - 30.0f), true);
            ImGui::Text("Player list");
            ImGui::EndChild(); // global-list_part_b

            colors[ImGuiCol_Button]                 = tmpCol;

            ImGui::EndChild(); // global-list

            static bool bInclude[4] = { true, true, true, true };

            ImGui::Text("Include: ");
            ImGui::SameLine(0, fSameSize);
            ImGui::Checkbox("Empty", &bInclude[0]);
            ImGui::SameLine(0, fSameSize);
            ImGui::Checkbox("Full", &bInclude[1]);
            ImGui::SameLine(0, fSameSize);
            ImGui::Checkbox("Locked", &bInclude[2]);
            ImGui::SameLine(0, fSameSize);
            ImGui::Checkbox("Other versions", &bInclude[3]);
            ImGui::SameLine(0, fSameSize);

            ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (ImGui::GetWindowContentRegionWidth() - 400.0f));
            ImGui::Button("Help", ImVec2(300, 60));
            ImGui::SameLine(0, fSameSize);
            if(ImGui::Button("Back", ImVec2(300, 60))) {
                mainmenu::showServerBrowser(-1);
            }

            ImGui::Text("Servers: 0");

            ImGui::End(); 
        }
    }
}

void mainmenu::showServerBrowser(uint position)
{
    mainmenu::g_iSpecialBrowserType = position;
}

void mainmenu::showAbout(bool s)
{
    mainmenu::g_bAboutActive = s;
}

void mainmenu::showSettings(bool s)
{
    mainmenu::g_bSettingsActive = s;
}

void mainmenu::processItem(int id)
{
    if(!mainmenu::g_bMenuActive) {
        return;
    }

    switch(id)
    {
       case MENU_ITEM_QUICK_CONNECT: 
       {
           mainmenu::showServerBrowser(0);
           break;
       }

       case MENU_ITEM_SERVER_BROWSER: {
           mainmenu::showServerBrowser(0);
           break;
       }

       case MENU_ITEM_HOST_GAME: {
           break;
       }

       case MENU_ITEM_MAP_EDITOR: {
           break;
       }

       case MENU_ITEM_SETTINGS: {
           mainmenu::showSettings(true);
           break;
       }

       case MENU_ITEM_ABOUT: {
           mainmenu::showAbout(true);
           break;
       }

       case MENU_ITEM_EXIT: {
           std::terminate();
           break;
       }

    }
}

void mainmenu::popTouch(float x, float y)
{
    if(!mainmenu::g_bMenuActive) {
        return;
    }

    mainmenu::processItem(mainmenu::g_iCurrentMenuItemSel);

    mainmenu::g_iCurrentMenuItemSel = 0;
}

void mainmenu::moveTouch(float x, float y)
{
    if(!mainmenu::g_bMenuActive || mainmenu::g_iSpecialBrowserType != -1 || mainmenu::g_bAboutActive || mainmenu::g_bSettingsActive) {
        return;
    }

    if(y > 500.0f && y < 575.0f) {
        mainmenu::g_iCurrentMenuItemSel = 1;
    }
    else if(y > 575.0f && y < 650.0f) {
        mainmenu::g_iCurrentMenuItemSel = 2;
    }
    else if(y > 650.0f && y < 725.0f) {
        mainmenu::g_iCurrentMenuItemSel = 3;
    }
    else if(y > 725.0f && y < 800.0f) {
        mainmenu::g_iCurrentMenuItemSel = 4;
    }
    else if(y > 800.0f && y < 875.0f) {
        mainmenu::g_iCurrentMenuItemSel = 5;
    }
    else if(y > 875.0f && y < 950.0f) {
        mainmenu::g_iCurrentMenuItemSel = 6;
    }
    else if(y > 950.0f && y < 1025.0f) {
        mainmenu::g_iCurrentMenuItemSel = 7;
    }
    else
    {
        mainmenu::g_iCurrentMenuItemSel = 0;
    }
}
