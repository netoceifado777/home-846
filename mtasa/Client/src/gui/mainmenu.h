#ifndef MAINMENU_H
#define MAINMENU_H

class mainmenu
{
private:
    static bool g_bMenuActive;
    static int g_iCurrentMenuItemSel;
    static uint g_iSpecialBrowserType;
    static float m_iXOff, m_iYOff, m_iMenuSizeX, m_iMenuSizeY;
    static bool g_bAboutActive;
    static bool g_bSettingsActive;
public:
    static void initialise();
    static void draw();
    static void renderServerBrowser();
    static void pushTouch(float, float);
    static void popTouch(float, float);
    static void moveTouch(float, float);
    static void processItem(int);
    static void toggleRender();
    static void showServerBrowser(uint);
    static void editStyle();
    static void showButtonBorder(bool);
    static void showChildBorder(bool);
    static void showAbout(bool);
    static void renderAbout();
    static void renderSettings();
    static void showSettings(bool);
};

#endif // MAINMENU_H