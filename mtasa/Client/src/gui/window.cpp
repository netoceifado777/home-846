/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "../core/core.h"
#include "imguiwrapper.h"
#include "button.h"
#include "window.h"

button* pButton[300];

window::window(uint32_t id, float x, float y, float sx, float sy, ImColor backgroundColor, ImColor textColor, bool bShow, bool bLockPad)
{
    g_fPosX = x;
    g_fPosY = y;
    g_fSizeX = sx;
    g_fSizeY = sy;
    g_bgColor = backgroundColor;
    g_textColor = textColor;
    g_bShow = bShow;
    g_ID = id;
    g_bLockPad = bLockPad;
    g_fFontSize = 28.0f;
    g_pFont = this->loadFont("arial-bold.ttf", g_fFontSize);

    pButton[0] = new button(11, "button", 300, 300, 120, 80, ImColor(0, 0, 0, 255), ImColor(255, 255, 255, 255), true);

    // hide game ui
    game::displayHUD(!bLockPad);
    game::displayWidgets(!bLockPad);
}

window::~window()
{
    g_fPosX = 0.0f;
    g_fPosY = 0.0f;
    g_fSizeX = 0.0f;
    g_fSizeY = 0.0f;
    g_bgColor = 0;
    g_textColor = 0;
    g_bShow = false;
    g_ID = 0;
    g_bLockPad = false;
    g_pFont = 0;
    g_fFontSize = 0.0f;
}

void window::setPadLocked(bool bLock)
{
    g_bLockPad = bLock;

    // hide game ui
    game::displayHUD(!bLock);
    game::displayWidgets(!bLock);
}

void window::setPosition(float x, float y)
{
    g_fPosX = x;
    g_fPosY = y;
}

void window::setSize(float x, float y)
{
    g_fSizeX = x;
    g_fSizeY = y;
}

void window::setBackgroundColor(ImColor color)
{
    g_bgColor = color;
}

void window::setTextColor(ImColor color)
{
    g_textColor = color;
}

void window::setVisibility(bool bShow)
{
    g_bShow = bShow;
}

ImFont* window::loadFont(char *font, float fontsize)
{
    ImGuiIO &io = ImGui::GetIO();

    char path[0xFF];
    sprintf(path, "%sMTASA/fonts/%s", utilities::getClientStorage(), font);

    static const ImWchar ranges[] =
    {
            0x0020, 0x00FF, 
            0x20AC, 0x20AC, 
            0x2122, 0x2122, 
            0x2196, 0x2196, 
            0x21D6, 0x21D6, 
            0x2B01, 0x2B01, 
            0x2B09, 0x2B09, 
            0x2921, 0x2922, 
            0x263A, 0x263A, 
            0x266A, 0x266A, 
            0x2191, 0x2191, 
            0x2193, 0x2193, 
            0x2022, 0x2022, 
            0x221A, 0x221A, 
            0x03C0, 0x03C0, 
            0x00F7, 0x00F7, 
            0x00D7, 0x00D7, 
            0x00B6, 0x00B6, 
            0x2206, 0x2206, 
            0x00A3, 0x00A3, 
            0x00A2, 0x00A2, 
            0x20AC, 0x20AC, 
            0x00A5, 0x00A5, 
            0x005E, 0x005E, 
            0x00B0, 0x00B0, 
            0x003D, 0x003D, 
            0x007B, 0x007B, 
            0x007D, 0x007D, 
            0x005C, 0x005C, 
            0x0025, 0x0025, 
            0x00A9, 0x00A9, 
            0x00AE, 0x00AE, 
            0x2122, 0x2122, 
            0x2713, 0x2713, 
            0x083F, 0x083F,
            0x005B, 0x005B, 
            0x005D, 0x005D, 
            0x3000, 0x30FF, 
            0x31F0, 0x31FF, 
            0xFF00, 0xFFEF, 
            0x2010, 0x205E, 
            0x0102, 0x0103,
            0x0110, 0x0111,
            0x0128, 0x0129,
            0x0168, 0x0169,
            0x01A0, 0x01A1,
            0x01AF, 0x01B0,
            0x1EA0, 0x1EF9,
            0x0400, 0x052F, 
            0x0400, 0x04FF, 
            0x0E00, 0x0E7F, 
            0x2DE0, 0x2DFF, 
            0xA640, 0xA69F, 
            0xF020, 0xF0FF, 
            0
    };

    ImFont* pFont = io.Fonts->AddFontFromFileTTF(path, fontsize, nullptr, ranges);

    return pFont;
}

static bool m_bNeedClearMousePos = true;

void window::drawButtons()
{
    for(int i = 0; i < 300; i++)
    {
        if(pButton[i]) {
            pButton[i]->draw(this);
        }
    }
}

void window::draw()
{
    if(!g_bShow) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    ImVec2 vecWindowInfoPos = ImVec2(g_fPosX, g_fPosY);
    ImVec2 vecWindowInfoSize = ImVec2(g_fPosX + g_fSizeX, g_fPosY + g_fSizeY);

    ImGuiWrapper::NewFrame();
	ImGui::NewFrame();

    ImVec2 vecDebug = ImVec2(30.0f, 30.0f);
    if(anticheat::getBuildType() == BUILD_TYPE_OFFICAL)
    {
        this->addText(23.0f, g_pFont, vecDebug, ImColor(0xFFFFFFFF), true, "mtasa-" CLIENT_VERSION "-official");
    }
    else
    {
        this->addText(23.0f, g_pFont, vecDebug, ImColor(0xFFFFFFFF), true, "mtasa-" CLIENT_VERSION "-custom");
    }

    window::drawButtons();
    mainmenu::draw();    
    keyboard::draw();

	ImGui::EndFrame();
	ImGui::Render();
	ImGuiWrapper::RenderDrawData(ImGui::GetDrawData());

    if(m_bNeedClearMousePos)
	{
		io.MousePos = ImVec2(-1, -1);
		m_bNeedClearMousePos = false;
	}
}

void window::fill(ImVec2& pos, ImVec2& size, ImU32 col)
{
    ImGui::GetBackgroundDrawList()->AddRectFilled(pos, size, col);
}

void window::addText(float font_size, ImFont* font, ImVec2& posCur, ImU32 col, bool bOutline, const char* text_begin, const char* text_end)
{
    /*
    if(!window::checkIn(posCur.x, posCur.y)) {
        // ERROR("Text cannot be added outside the window borders");
        return;
    }
    */

    int iOffset = 2;

    if (bOutline)
    {
        // left
        posCur.x -= iOffset;
        ImGui::GetBackgroundDrawList()->AddText(font, font_size, posCur, ImColor(0,0,0,255), text_begin, text_end);
        posCur.x += iOffset;
        // right
        posCur.x += iOffset;
        ImGui::GetBackgroundDrawList()->AddText(font, font_size, posCur, ImColor(0,0,0,255), text_begin, text_end);
        posCur.x -= iOffset;
        // above
        posCur.y -= iOffset;
        ImGui::GetBackgroundDrawList()->AddText(font, font_size, posCur, ImColor(0,0,0,255), text_begin, text_end);
        posCur.y += iOffset;
        // below
        posCur.y += iOffset;
        ImGui::GetBackgroundDrawList()->AddText(font, font_size, posCur, ImColor(0,0,0,255), text_begin, text_end);
        posCur.y -= iOffset;
    }

    ImGui::GetBackgroundDrawList()->AddText(font, font_size, posCur, col, text_begin, text_end);
}

bool window::checkIn(float x, float y)
{
    return (x < g_fPosX || y < g_fPosY ||
            x > (g_fPosX + g_fSizeX) || y > (g_fPosY + g_fSizeY)) ? false : true;
}

void window::processPop(float x, float y)
{
    for(int i = 0; i < 300; i++)
    {
        if(pButton[i]) {
            pButton[i]->popTouch(x, y);
        }
    }

    if(g_ID == 1) {
        mainmenu::popTouch(x, y);
    }
}

void window::processPush(float x, float y)
{
    for(int i = 0; i < 300; i++)
    {
        if(pButton[i]) {
            pButton[i]->pushTouch(x, y);
        }
    }

    if(g_ID == 1) {
        mainmenu::pushTouch(x, y);
    }
}

void window::processMove(float x, float y)
{ 
    for(int i = 0; i < 300; i++)
    {
        if(pButton[i]) {
            pButton[i]->moveTouch(x, y);
        }
    }

    if(g_ID == 1) {
        mainmenu::moveTouch(x, y);
    }
}

bool window::touch(float x, float y, uint8_t type)
{
    if(!g_bShow) {
        return true;
    }

    if(*(uint8_t*)(SA(0x8C9BA3))) {
        return true;
    }

    if(!window::checkIn(x, y)) {
        // LOG("touched out window");
        return true;
    }

    ImGuiIO& io = ImGui::GetIO();

    static bool bWannaTouch = false;

    switch(type)
    {
        case TOUCH_POP:
        {
            io.MouseDown[0] = false;
		    m_bNeedClearMousePos = true;

            window::processPop(x, y);
            bWannaTouch = true;

            break;
        }

        case TOUCH_PUSH:
        {
            if(bWannaTouch) {
               window::processPush(x, y);
            }

            io.MousePos = ImVec2(x, y);
		    io.MouseDown[0] = true;

            bWannaTouch = false;

            break;
        }

        case TOUCH_MOVE:
        {
            window::processMove(x, y);

            io.MousePos = ImVec2(x, y);

            bWannaTouch = false;

            break;
        }
    }

    return !g_bLockPad;
}