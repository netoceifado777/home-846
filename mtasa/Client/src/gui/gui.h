#ifndef GUI_H
#define GUI_H

#include "imguiwrapper.h"

void InitGUI();
void DestroyGUI();

#endif // GUI_H