/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "core.h"

modmanager* core::m_pModManager;
game* core::m_pGame;

uint32_t GTASA__Thread_Worker(int core);

void core::initialize()
{
    LOG("initializing core..");

    m_pGame = new game();
    m_pModManager = new modmanager();
}

void core::OnCrashAverted(uint uiId)
{
    crashdump::OnCrashAverted(uiId);
}

void core::OnEnterCrashZone(uint uiId)
{
    crashdump::OnEnterCrashZone(uiId);
}

bool core::GetDebugIdEnabled(uint uiDebugId)
{
    return false;
}

void core::LogEvent(uint uiDebugId, const char* szType, const char* szContext, const char* szBody, uint uiAddReportLogId)
{

}

game* core::getGame()
{
    return core::m_pGame;
}

modmanager* core::getModManager()
{
    return core::m_pModManager;
}