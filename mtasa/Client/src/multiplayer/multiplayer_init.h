#ifndef MULTIPLAYER_INIT_H
#define MULTIPLAYER_INIT_H

#define MAX_FPS 90
#define MIN_FPS 30

 void InitMultiPlayer();
 void InitGlobalHooksAndPatches();
 void OnCrashAverted(uint uiId);
 void OnEnterCrashZone(uint uiId);
 bool GetDebugIdEnabled(uint uiDebugId);
 void LogEvent(uint uiDebugId, const char* szType, const char* szContext, const char* szBody, uint uiAddReportLogId);

#endif // MULTIPLAYER_INIT_H