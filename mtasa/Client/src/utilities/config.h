#pragma once

#include "../../vendor/SimpleIni/SimpleIni.h"

#define INVALID_CONFIG_VALUE 0xFFFF

class Config
{
public:
    static CSimpleIniA m_pCurrentConfig;
    static bool        m_bConfigLoaded;
    static char        m_szConfigName[0xFF];

public:
    static void Create(const char* name);
    static void Load(const char* name);
    static void Unload();

    static const char* GetValueString(const char* section, const char* key);
    static int GetValueInt(const char* section, const char* key);
    static float GetValueFloat(const char* section, const char* key);

    static void SetValueString(const char* section, const char* key, const char* value);
    static void SetValueInt(const char* section, const char* key, int value);
    static void SetValueFloat(const char* section, const char* key, float value);

    static void CreateSection(const char* section);
    static void DeleteSection(const char* section);
    static void DeleteKey(const char* section, const char* key);

    static void SaveAll();
};