/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"

uint32_t utilities::getTickCount()
{
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return (tv.tv_sec*1000+tv.tv_usec/1000);
}

void utilities::cpToUtf8(char *out, const char *in, unsigned int len)
{
    static const int table[128] = 
    {                    
        // 80
        0x82D0,     0x83D0,     0x9A80E2,   0x93D1,     0x9E80E2,   0xA680E2,   0xA080E2,   0xA180E2,
        0xAC82E2,   0xB080E2,   0x89D0,     0xB980E2,   0x8AD0,     0x8CD0,     0x8BD0,     0x8FD0,
        // 90
        0x92D1,     0x9880E2,   0x9980E2,   0x9C80E2,   0x9D80E2,   0xA280E2,   0x9380E2,   0x9480E2,
        0,          0xA284E2,   0x99D1,     0xBA80E2,   0x9AD1,     0x9CD1,     0x9BD1,     0x9FD1,
        // A0
        0xA0C2,     0x8ED0,     0x9ED1,     0x88D0,     0xA4C2,     0x90D2,     0xA6C2,     0xA7C2,              
        0x81D0,     0xA9C2,     0x84D0,     0xABC2,     0xACC2,     0xADC2,     0xAEC2,     0x87D0,
        // B0
        0xB0C2,     0xB1C2,     0x86D0,     0x96D1,     0x91D2,     0xB5C2,     0xB6C2,     0xB7C2,              
        0x91D1,     0x9684E2,   0x94D1,     0xBBC2,     0x98D1,     0x85D0,     0x95D1,     0x97D1,
        // C0
        0x90D0,     0x91D0,     0x92D0,     0x93D0,     0x94D0,     0x95D0,     0x96D0,     0x97D0,
        0x98D0,     0x99D0,     0x9AD0,     0x9BD0,     0x9CD0,     0x9DD0,     0x9ED0,     0x9FD0,
        // D0
        0xA0D0,     0xA1D0,     0xA2D0,     0xA3D0,     0xA4D0,     0xA5D0,     0xA6D0,     0xA7D0,
        0xA8D0,     0xA9D0,     0xAAD0,     0xABD0,     0xACD0,     0xADD0,     0xAED0,     0xAFD0,
        // E0
        0xB0D0,     0xB1D0,     0xB2D0,     0xB3D0,     0xB4D0,     0xB5D0,     0xB6D0,     0xB7D0,
        0xB8D0,     0xB9D0,     0xBAD0,     0xBBD0,     0xBCD0,     0xBDD0,     0xBED0,     0xBFD0,
        // F0
        0x80D1,     0x81D1,     0x82D1,     0x83D1,     0x84D1,     0x85D1,     0x86D1,     0x87D1,
        0x88D1,     0x89D1,     0x8AD1,     0x8BD1,     0x8CD1,     0x8DD1,     0x8ED1,     0x8FD1
    };

    int count = 0;

    while (*in)
    {
        if(len && (count >= len)) {
            break;
        }

        if (*in & 0x80)
        {
            register int v = table[(int)(0x7f & *in++)];
            if (!v) {
                continue;   
            }

            *out++ = (char)v;
            *out++ = (char)(v >> 8);

            if (v >>= 16) {
                *out++ = (char)v;
            }
        }
        else 
        {
            // ASCII
            *out++ = *in++;
        }

        count++;
    }

    *out = 0;
}

uintptr_t utilities::getLibraryHandle(const char* library)
{
    char filename[0xFF] = {0},
    buffer[2048] = {0};
    FILE *fp = 0;
    uintptr_t address = 0;

    sprintf( filename, "/proc/%d/maps", getpid() );

    fp = fopen( filename, "rt" );
    if(fp == 0)
    {
        goto done;
    }

    while(fgets(buffer, sizeof(buffer), fp))
    {
        if( strstr( buffer, library ) )
        {
            address = (uintptr_t)strtoul( buffer, 0, 16 );
            break;
        }
    }

    done:

    if(fp)
      fclose(fp);

    return address;
}

uintptr_t utilities::findMethod(const char *libname, const char *name)
{
    auto result = dlsym(dlopen(libname, RTLD_NOW), name);

    Dl_info info;
    dladdr((void*)result, &info);

    return ((uintptr_t)info.dli_saddr - utilities::getLibraryHandle(libname));
}

char* utilities::g_szStorage = nullptr;
void utilities::setClientStorage(char* storage)
{
    LOG("Storage: %s", storage);
    utilities::g_szStorage = storage;
}

char* utilities::getClientStorage()
{
    return utilities::g_szStorage;
}

unsigned utilities::elfhash(const char *_name) {
   const unsigned char *name = (const unsigned char *) _name;
   unsigned h = 0, g;
   while(*name) {
       h = (h << 4) + *name++;
       g = h & 0xf0000000;
       h ^= g;
       h ^= g >> 24;
   }
   return h;
}

uint32_t utilities::gnuhash(const char *_name) {
    uint32_t h = 5381;
    const uint8_t* name = reinterpret_cast<const uint8_t*>(_name);
    while (*name != 0) {
        h += (h << 5) + *name++; // h*33 + c = h + h * 32 + c = h + h << 5 + c
    }
    return h;
}

uintptr_t HOOK_GetTexture(const char* a1);
uintptr_t utilities::loadTextureFromDB(const char* dbname, const char *texture)
{
    // TextureDatabaseRuntime::GetDatabase(dbname)
	uintptr_t db_handle = (( uintptr_t (*)(const char*))(SA(0x1BF530+1)))(dbname);
	if(!db_handle)
	{
		LOG("Error: Database not found! (%s)", dbname);
		return 0;
	}
	// TextureDatabaseRuntime::Register(db)
	(( void (*)(uintptr_t))(SA(0x1BE898+1)))(db_handle);
	uintptr_t tex = HOOK_GetTexture(texture);

	if(!tex) LOG("Error: Texture (%s) not found in database (%s)", dbname, texture);

	// TextureDatabaseRuntime::Unregister(db)
	(( void (*)(uintptr_t))(SA(0x1BE938+1)))(db_handle);

	return tex;
}

uintptr_t utilities::loadTextureFromTxd(const char* database, const char *texture)
{
	uintptr_t pRwTexture = 0;

	int g_iTxdSlot = ((int (__fastcall *)(const char *))(SA(0x55BB85)))(database); // CTxdStore::FindTxdSlot
	if(g_iTxdSlot == -1)
	{
		LOG("no txd");
	}
	else
	{
		((void (*)(void))(SA(0x55BD6D)))(); // CTxdStore::PushCurrentTxd
		((void (__fastcall *)(int, uint32_t))(SA(0x55BCDD)))(g_iTxdSlot, 0);
		((void (__fastcall *)(uintptr_t *, const char *))(SA(0x551855)))(&pRwTexture, texture); // CSprite2d::SetTexture
		((void (*)(void))(SA(0x55BDA9)))(); // CTxdStore::PopCurrentTxd
	}

	return pRwTexture;
}

