/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "config.h"
#include "game/game.h"

CSimpleIniA Config::m_pCurrentConfig;
bool        Config::m_bConfigLoaded;
char        Config::m_szConfigName[0xFF];

void Config::Load(const char* name)
{
    Config::m_bConfigLoaded = false;
    
    memset(Config::m_szConfigName, 0, sizeof(m_szConfigName));
    strcpy(Config::m_szConfigName, name);

    Config::m_pCurrentConfig.SetUnicode();

    char path[0xFF] = { 0 };
	sprintf(path, "%sMTASA/%s", utilities::getClientStorage(), name);

    SI_Error rc = Config::m_pCurrentConfig.LoadFile(path);

	if (rc >= 0) { 
        Config::m_bConfigLoaded = true;
    }
}

void Config::Unload()
{
    memset(Config::m_szConfigName, 0, sizeof(m_szConfigName));
    Config::m_bConfigLoaded = false;
}

void Config::Create(const char* name)
{
    char path[0xFF] = { 0 };
	sprintf(path, "%sMTASA/%s", utilities::getClientStorage(), name);

    static FILE* flCfg = 0;
    flCfg = fopen(path, "a");
    fprintf(flCfg, "# MTA:SA Configuration File: %s ", name);
	fflush(flCfg);
}

const char* Config::GetValueString(const char* section, const char* key)
{
    if(!Config::m_bConfigLoaded) {
        return 0;
    }

    return (const char*)Config::m_pCurrentConfig.GetValue(section, key, 0);
}

int Config::GetValueInt(const char* section, const char* key)
{
    if(!Config::m_bConfigLoaded) {
        return INVALID_CONFIG_VALUE;
    }

    return (int)atoi(Config::m_pCurrentConfig.GetValue(section, key, 0));
}

float Config::GetValueFloat(const char* section, const char* key)
{
    if(!Config::m_bConfigLoaded) {
        return INVALID_CONFIG_VALUE;
    }

    return (float)atof(Config::m_pCurrentConfig.GetValue(section, key, 0));
}

void Config::SetValueString(const char* section, const char* key, const char* value)
{
    if(!Config::m_bConfigLoaded) {
        return;
    }

    Config::m_pCurrentConfig.SetValue(section, key, value);
    Config::SaveAll();
}

void Config::SetValueInt(const char* section, const char* key, int value)
{
    if(!Config::m_bConfigLoaded) {
        return;
    }

    char buf[0xFF] = { 0 };
    sprintf(buf, "%d", value);
    Config::m_pCurrentConfig.SetValue(section, key, buf);
    Config::SaveAll();
}

void Config::SetValueFloat(const char* section, const char* key, float value)
{
    if(!Config::m_bConfigLoaded) {
        return;
    }

    char buf[0xFF] = { 0 };
    sprintf(buf, "%f", value);

    Config::m_pCurrentConfig.SetValue(section, key, buf);
    Config::SaveAll();
}

void Config::CreateSection(const char* section)
{
    if(!Config::m_bConfigLoaded) {
        return;
    }

    Config::m_pCurrentConfig.SetValue(section, nullptr, nullptr);
    Config::SaveAll();
}

void Config::DeleteSection(const char* section)
{   
    if(!Config::m_bConfigLoaded) {
        return;
    }

    Config::m_pCurrentConfig.Delete(section, 0);
    Config::SaveAll();
}

void Config::DeleteKey(const char* section, const char* key)
{
    if(!Config::m_bConfigLoaded) {
        return;
    }

    Config::m_pCurrentConfig.Delete(section, key);
    Config::SaveAll();
}   

void Config::SaveAll()
{
    char path[0xFF] = { 0 };
	sprintf(path, "%sMTASA/%s", utilities::getClientStorage(), Config::m_szConfigName);
    Config::m_pCurrentConfig.SaveFile(path);
}