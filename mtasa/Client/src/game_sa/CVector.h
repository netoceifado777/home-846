#pragma once
#include "../main.h"

#pragma pack(push, 1)
class CVector {
public:
    float x, y, z;
};
#pragma pack(pop)