#include "../../main.h"
#include "RenderWare.h"
#include "../../multiplayer/multiplayer.h"

RsGlobalType* RsGlobal;

RwCamera* (*RwCameraBeginUpdate)(RwCamera *camera);
RwCamera* (*RwCameraEndUpdate)(RwCamera *camera);
RwCamera* (*RwCameraShowRaster)(RwCamera *camera, void *pDev, RwUInt32 flags);

RwRaster *(*RwRasterCreate)(RwInt32 width, RwInt32 height, RwInt32 depth, RwInt32 flags);
RwBool (*RwRasterDestroy)(RwRaster * raster);
RwRaster *(*RwRasterGetOffset)(RwRaster *raster, RwInt16 *xOffset, RwInt16 *yOffset);
RwInt32 (*RwRasterGetNumLevels)(RwRaster * raster);
RwRaster *(*RwRasterSubRaster)(RwRaster * subRaster, RwRaster * raster, RwRect * rect);
RwRaster *(*RwRasterRenderFast)(RwRaster * raster, RwInt32 x, RwInt32 y);
RwRaster *(*RwRasterRender)(RwRaster * raster, RwInt32 x, RwInt32 y);
RwRaster *(*RwRasterRenderScaled)(RwRaster * raster, RwRect * rect);
RwRaster *(*RwRasterPushContext)(RwRaster * raster);
RwRaster *(*RwRasterPopContext)(void);
RwRaster *(*RwRasterGetCurrentContext)(void);
RwBool (*RwRasterClear)(RwInt32 pixelValue);
RwBool (*RwRasterClearRect)(RwRect * rpRect, RwInt32 pixelValue);
RwRaster *(*RwRasterShowRaster)(RwRaster * raster, void *dev, RwUInt32 flags);
RwUInt8 *(*RwRasterLock)(RwRaster * raster, RwUInt8 level, RwInt32 lockMode);
RwRaster *(*RwRasterUnlock)(RwRaster * raster);
RwUInt8 *(*RwRasterLockPalette)(RwRaster * raster, RwInt32 lockMode);
RwRaster *(*RwRasterUnlockPalette)(RwRaster * raster);
RwImage *(*RwImageCreate)(RwInt32 width, RwInt32 height, RwInt32 depth);
RwBool (*RwImageDestroy)(RwImage * image);
RwImage *(*RwImageAllocatePixels)(RwImage * image);
RwImage *(*RwImageFreePixels)(RwImage * image);
RwImage *(*RwImageCopy)(RwImage * destImage, const RwImage * sourceImage);
RwImage *(*RwImageResize)(RwImage * image, RwInt32 width, RwInt32 height);
RwImage *(*RwImageApplyMask)(RwImage * image, const RwImage * mask);
RwImage *(*RwImageMakeMask)(RwImage * image);
RwImage *(*RwImageReadMaskedImage)(const RwChar * imageName, const RwChar * maskname);
RwImage *(*RwImageRead)(const RwChar * imageName);
RwImage *(*RwImageWrite)(RwImage * image, const RwChar * imageName);
RwImage *(*RwImageSetFromRaster)(RwImage *image, RwRaster *raster);
RwRaster *(*RwRasterSetFromImage)(RwRaster *raster, RwImage *image);
RwRaster *(*RwRasterRead)(const RwChar *filename);
RwRaster *(*RwRasterReadMaskedRaster)(const RwChar *filename, const RwChar *maskname);
RwImage *(*RwImageFindRasterFormat)(RwImage *ipImage, RwInt32 nRasterType, RwInt32 *npWidth, RwInt32 *npHeight, RwInt32 *npDepth, RwInt32 *npFormat);
RwTexture *(*RwTextureCreate)(RwRaster* raster);

/* rwlpcore.h */
RwStream* (*_rwStreamInitialize)(RwStream* stream, RwBool rwOwned, RwStreamType type, RwStreamAccessType accessType, const void* pData);
RwStream* (*RwStreamOpen)(RwStreamType type, RwStreamAccessType accessType, const void* pData); 
RwBool (*RwStreamClose)(RwStream* stream, void* pData); 
RwUInt32 (*RwStreamRead)(RwStream* stream, void* buffer, RwUInt32 length); 
RwStream* (*RwStreamWrite)(RwStream* stream, const void* buffer, RwUInt32 length); 
RwStream* (*RwStreamSkip)(RwStream* stream, RwUInt32 offset); 
RwReal (*RwIm2DGetNearScreenZ)(void);
RwReal (*RwIm2DGetFarScreenZ)(void);
RwBool (*RwRenderStateGet)(RwRenderState state, void *value);
RwBool (*RwRenderStateSet)(RwRenderState state, void *value);
RwBool (*RwIm2DRenderLine)(RwIm2DVertex *vertices, RwInt32 numVertices, RwInt32 vert1, RwInt32 vert2);
RwBool (*RwIm2DRenderTriangle)(RwIm2DVertex *vertices, RwInt32 numVertices, RwInt32 vert1, RwInt32 vert2, RwInt32 vert3 );
RwBool (*RwIm2DRenderPrimitive)(RwPrimitiveType primType, RwIm2DVertex *vertices, RwInt32 numVertices);
RwBool (*RwIm2DRenderIndexedPrimitive)(RwPrimitiveType primType, RwIm2DVertex *vertices, RwInt32 numVertices, RwImVertexIndex *indices, RwInt32 numIndices);

/* rtpng.h */
RwImage *(*RtPNGImageWrite)(RwImage* image, const RwChar* imageName);
RwImage *(*RtPNGImageRead)(const RwChar* imageName);

void InitRenderWareFunctions()
{
	LOG("initializing RenderWare..");

	RsGlobal = (RsGlobalType *)(multiplayer::VAR_RS_GLOBAL);
	*(void **)(&RwCameraBeginUpdate) 			= (void *)(multiplayer::FUNC_RwCameraBeginUpdate + 1);
	*(void **)(&RwCameraEndUpdate) 				= (void *)(multiplayer::FUNC_RwCameraEndUpdate + 1);
	*(void **)(&RwCameraShowRaster)				= (void *)(multiplayer::FUNC_RwCameraShowRaster + 1);
	*(void **)(&RwRasterCreate) 				= (void *)(multiplayer::FUNC_RwRasterCreate + 1);
	*(void **)(&RwRasterDestroy) 				= (void *)(multiplayer::FUNC_RwRasterDestroy + 1);
	*(void **)(&RwRasterGetOffset) 				= (void *)(multiplayer::FUNC_RwRasterGetOffset + 1);
	*(void **)(&RwRasterGetNumLevels) 			= (void *)(multiplayer::FUNC_RwRasterGetNumLevels + 1);
	*(void **)(&RwRasterSubRaster) 				= (void *)(multiplayer::FUNC_RwRasterSubRaster + 1);
	*(void **)(&RwRasterRenderFast)				= (void *)(multiplayer::FUNC_RwRasterRenderFast + 1);
	*(void **)(&RwRasterRender)					= (void *)(multiplayer::FUNC_RwRasterRender + 1);
	*(void **)(&RwRasterRenderScaled)			= (void *)(multiplayer::FUNC_RwRasterRenderScaled + 1);
	*(void **)(&RwRasterPushContext)			= (void *)(multiplayer::FUNC_RwRasterPushContext + 1);
	*(void **)(&RwRasterPopContext)				= (void *)(multiplayer::FUNC_RwRasterPopContext + 1);
	*(void **)(&RwRasterGetCurrentContext)		= (void *)(multiplayer::FUNC_RwRasterGetCurrentContext + 1);
	*(void **)(&RwRasterClear)					= (void *)(multiplayer::FUNC_RwRasterClear + 1);
	*(void **)(&RwRasterClearRect)				= (void *)(multiplayer::FUNC_RwRasterClearRect + 1);
	*(void **)(&RwRasterShowRaster)				= (void *)(multiplayer::FUNC_RwRasterShowRaster + 1);
	*(void **)(&RwRasterLock)					= (void *)(multiplayer::FUNC_RwRasterLock + 1);
	*(void **)(&RwRasterUnlock)					= (void *)(multiplayer::FUNC_RwRasterUnlock + 1);
	*(void **)(&RwRasterLockPalette)			= (void *)(multiplayer::FUNC_RwRasterLockPalette + 1);
	*(void **)(&RwRasterUnlockPalette)			= (void *)(multiplayer::FUNC_RwRasterUnlockPalette + 1);
	*(void **)(&RwImageCreate)					= (void *)(multiplayer::FUNC_RwImageCreate + 1);
	*(void **)(&RwImageDestroy)					= (void *)(multiplayer::FUNC_RwImageDestroy + 1);
	*(void **)(&RwImageAllocatePixels)			= (void *)(multiplayer::FUNC_RwImageAllocatePixels + 1);
	*(void **)(&RwImageFreePixels)				= (void *)(multiplayer::FUNC_RwImageFreePixels + 1);
	*(void **)(&RwImageCopy)					= (void *)(multiplayer::FUNC_RwImageCopy + 1);
	*(void **)(&RwImageResize)					= (void *)(multiplayer::FUNC_RwImageResize + 1);
	*(void **)(&RwImageApplyMask)				= (void *)(multiplayer::FUNC_RwImageApplyMask + 1);
	*(void **)(&RwImageMakeMask)				= (void *)(multiplayer::FUNC_RwImageMakeMask + 1);
	*(void **)(&RwImageReadMaskedImage)			= (void *)(multiplayer::FUNC_RwImageReadMaskedImage + 1);
	*(void **)(&RwImageRead)					= (void *)(multiplayer::FUNC_RwImageRead + 1);
	*(void **)(&RwImageWrite)					= (void *)(multiplayer::FUNC_RwImageWrite + 1);
	*(void **)(&RwImageSetFromRaster)			= (void *)(multiplayer::FUNC_RwImageSetFromRaster + 1);
	*(void **)(&RwRasterSetFromImage)			= (void *)(multiplayer::FUNC_RwRasterSetFromImage + 1);
	*(void **)(&RwRasterRead)					= (void *)(multiplayer::FUNC_RwRasterRead + 1);
	*(void **)(&RwRasterReadMaskedRaster)		= (void *)(multiplayer::FUNC_RwRasterReadMaskedRaster + 1);
	*(void **)(&RwImageFindRasterFormat)		= (void *)(multiplayer::FUNC_RwImageFindRasterFormat + 1);
	*(void **)(&RwTextureCreate)				= (void *)(multiplayer::FUNC_RwTextureCreate + 1);
	*(void **)(&_rwStreamInitialize)			= (void *)(multiplayer::FUNC__rwStreamInitialize + 1);
	*(void **)(&RwStreamOpen)					= (void *)(multiplayer::FUNC_RwStreamOpen + 1);
	*(void **)(&RwStreamClose)					= (void *)(multiplayer::FUNC_RwStreamClose + 1);
	*(void **)(&RwStreamRead)					= (void *)(multiplayer::FUNC_RwStreamRead + 1);
	*(void **)(&RwStreamWrite)					= (void *)(multiplayer::FUNC_RwStreamWrite + 1);
	*(void **)(&RwStreamSkip)					= (void *)(multiplayer::FUNC_RwStreamSkip + 1);
	*(void **)(&RwIm2DGetNearScreenZ)			= (void *)(multiplayer::FUNC_RwIm2DGetNearScreenZ + 1);
	*(void **)(&RwIm2DGetFarScreenZ)			= (void *)(multiplayer::FUNC_RwIm2DGetFarScreenZ + 1);
	*(void **)(&RwRenderStateGet)				= (void *)(multiplayer::FUNC_RwRenderStateGet + 1);
	*(void **)(&RwRenderStateSet)				= (void *)(multiplayer::FUNC_RwRenderStateSet + 1);
	*(void **)(&RwIm2DRenderLine)				= (void *)(multiplayer::FUNC_RwIm2DRenderLine + 1);
	*(void **)(&RwIm2DRenderTriangle)			= (void *)(multiplayer::FUNC_RwIm2DRenderTriangle + 1);
	*(void **)(&RwIm2DRenderPrimitive)			= (void *)(multiplayer::FUNC_RwIm2DRenderPrimitive + 1);
	*(void **)(&RwIm2DRenderIndexedPrimitive)	= (void *)(multiplayer::FUNC_RwIm2DRenderIndexedPrimitive + 1);
	*(void **)(&RtPNGImageWrite)				= (void *)(multiplayer::FUNC_RtPNGImageWrite + 1);
	*(void **)(&RtPNGImageRead)					= (void *)(multiplayer::FUNC_RtPNGImageRead + 1);
}