#include "../main.h"

void CWorld::Add(uintptr_t entity)
{
    ((void (*)(uintptr_t))(SA(utilities::findMethod("libGTASA.so", "_ZN6CWorld3AddEP7CEntity"))))(entity);
}

void CWorld::Remove(uintptr_t entity)
{
    ((void (*)(uintptr_t))(SA(utilities::findMethod("libGTASA.so", "_ZN6CWorld6RemoveEP7CEntity"))))(entity);
}